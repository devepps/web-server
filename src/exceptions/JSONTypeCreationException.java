package exceptions;

import data.exceptions.CustomException;

public class JSONTypeCreationException extends CustomException {

	private static final long serialVersionUID = 1L;

	public JSONTypeCreationException(Class<?> superClass) {
		super("An error occurred while reading JSON type class : " + superClass);
	}

	public JSONTypeCreationException(Class<?> superClass, Throwable e) {
		super(e, "An error occurred while reading JSON type class : " + superClass);
	}

}
