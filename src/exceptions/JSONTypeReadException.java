package exceptions;

import com.github.cliftonlabs.json_simple.JsonObject;

import data.exceptions.CustomException;

public class JSONTypeReadException extends CustomException {

	private static final long serialVersionUID = 1L;

	public JSONTypeReadException(Object obj) {
		super("An error occurred while reading JSON type of Object : " + obj);
	}

	public JSONTypeReadException(Object obj, Throwable e) {
		super(e, "An error occurred while reading of Object : " + obj);
	}

	public JSONTypeReadException(JsonObject jsonObject, String key) {
		super("An error occurred while reading of Json Object : " + jsonObject + " on key : " + key);
	}

	public JSONTypeReadException(JsonObject jsonObject, String key, Throwable e) {
		super(e, "An error occurred while reading of Json Object : " + jsonObject + " on key : " + key);
	}

}
