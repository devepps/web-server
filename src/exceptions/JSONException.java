package exceptions;

import data.exceptions.CustomException;

public class JSONException extends CustomException {

	private static final long serialVersionUID = 1L;

	public JSONException(String json) {
		super("An error occurred while reading JSON : " + json);
	}

}
