package server.logger;
import config.ConfigEnvironment;
import logger.main.Logger;
import logger.main.prefixs.TimePrefix;

public class ServerLogger extends Logger {

	private static final boolean debug = Boolean.parseBoolean(ConfigEnvironment.getProperty("Status.HTTPS-Server.Debug"));

	private static final ServerLogger logger = new ServerLogger();

	public ServerLogger() {
		super("HTTPS-Server", new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"));
	}
	
	public static void debug(String msg) {
		if(!debug) return;
		logger.println("[debug] " + msg);
	}

	public static void debug(String msg, long clientId) {
		if(!debug) return;
		logger.println("[debug] " + "[" + clientId + "] " + msg);
	}

	public static void debug(String msg, String userName) {
		if(!debug) return;
		logger.println("[debug] " + "[" + userName + "] " + msg);
	}
	
	public static void debug(String msg, Throwable e) {
		if(!debug) return;
		logger.println("[debug] " + msg + "->  An error occured: " + e.getMessage());
	}

	public static void debug(String msg, long clientId, Throwable e) {
		if(!debug) return;
		logger.println("[debug] " + "[" + clientId + "] " + msg + "->  An error occured: " + e.getMessage());
	}

	public static void debug(String msg, String userName, Throwable e) {
		if(!debug) return;
		logger.println("[debug] " + "[" + userName + "] " + msg + "->  An error occured: " + e.getMessage());
	}
	
	public static void info(String msg) {
		logger.println("[info] " + msg);
	}

	public static void info(String msg, long clientId) {
		logger.println("[info] " + "[" + clientId + "] " + msg);
	}

	public static void info(String msg, String userName) {
		logger.println("[info] " + "[" + userName + "] " + msg);
	}
}
