package server.data.requestAcceptor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import com.sun.net.httpserver.HttpExchange;

import collection.sync.SyncArrayList;
import json.JSONReader;
import server.data.exceptions.RequestAcceptorCreationException;
import server.data.interceptor.after.After;
import server.data.interceptor.after.AfterWorkerBase;
import server.data.interceptor.before.Before;
import server.data.interceptor.before.BeforeWorkerBase;
import server.data.interceptor.interceptor.Intercept;
import server.data.interceptor.interceptor.InterceptedResponse;
import server.data.interceptor.interceptor.Interceptor;
import server.data.parameter.ParameterManager;
import server.data.responseMessage.ResponseMessageBase;
import server.data.type.ContentType;
import server.data.type.RequestType;

@SuppressWarnings("hiding")
public abstract class RequestAcceptorBaseData <ContentType, ResponseType extends ResponseMessageBase<ContentType>> {

	private String path;
	private RequestType requestType;
	private Method method;
	private Object controller;
	private ParameterManager parameterManager;
	private boolean acceptsRawData;

	private final SyncArrayList<BeforeWorkerBase<ContentType, ResponseMessageBase<ContentType>>> beforeWorkers = new SyncArrayList<>();
	private final SyncArrayList<AfterWorkerBase<ContentType, ResponseMessageBase<ContentType>>> afterWorkers = new SyncArrayList<>();
	private final SyncArrayList<Interceptor<ContentType>> interceptors = new SyncArrayList<>();

	@SuppressWarnings("unchecked")
	public RequestAcceptorBaseData(JSONReader jsonReader, String path, RequestType requestType, Method method, boolean acceptsRawData) {
		this.path = path;
		this.requestType = requestType;
		this.method = method;
		this.acceptsRawData = acceptsRawData;
		try {
			method.setAccessible(true);
			this.controller = method.getDeclaringClass().getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new RequestAcceptorCreationException(method);
		}

		// <=== get Before ===>
		this.beforeWorkers
				.addAll((Collection<? extends BeforeWorkerBase<ContentType, ResponseMessageBase<ContentType>>>) loadBeforeWorker(
						method));

		// <=== get After ===>
		this.afterWorkers
				.addAll((Collection<? extends AfterWorkerBase<ContentType, ResponseMessageBase<ContentType>>>) loadAfterWorker(
						method));
		// <=== get Interceptors ===>
		Intercept intercept = method.getDeclaredAnnotation(Intercept.class);
		if (intercept != null && intercept.interceptors() != null && intercept.interceptors().length > 0) {
			for (Class<? extends Interceptor<?>> interceptorClass : intercept.interceptors()) {
				try {
					this.interceptors.add((Interceptor<ContentType>) interceptorClass.getConstructor().newInstance());
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		this.parameterManager = new ParameterManager(jsonReader, method);
		this.initialize();
	}

	protected abstract void initialize();

	@SuppressWarnings("unchecked")
	private ArrayList<BeforeWorkerBase<byte[], ResponseMessageBase<byte[]>>> loadBeforeWorker(Method method) {
		Before before = method.getDeclaredAnnotation(Before.class);
		ArrayList<BeforeWorkerBase<byte[], ResponseMessageBase<byte[]>>> beforeWorkers = new ArrayList<>();

		if (before != null && before.befores() != null && before.befores().length > 0) {
			for (Class<? extends BeforeWorkerBase<?, ?>> beforeWorkerClass : before.befores()) {
				try {
					BeforeWorkerBase<byte[], ResponseMessageBase<byte[]>> beforeWorker = (BeforeWorkerBase<byte[], ResponseMessageBase<byte[]>>) beforeWorkerClass.getConstructor().newInstance();
					for (int i = 0; i < beforeWorkers.size(); i++) {
						BeforeWorkerBase<byte[], ResponseMessageBase<byte[]>> current = beforeWorkers.get(i);
						if (current.order() > beforeWorker.order()) {
							beforeWorkers.add(i, beforeWorker);
							beforeWorker = null;
							break;
						}
					}
					if (beforeWorker != null) {
						beforeWorkers.add(beforeWorker);						
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
		}
		return beforeWorkers;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<AfterWorkerBase<byte[], ResponseMessageBase<byte[]>>> loadAfterWorker(Method method) {
		After after = method.getDeclaredAnnotation(After.class);
		ArrayList<AfterWorkerBase<byte[], ResponseMessageBase<byte[]>>> afterWorkers = new ArrayList<>();

		if (after != null && after.afters() != null && after.afters().length > 0) {
			for (Class<? extends AfterWorkerBase<?, ?>> afterWorkerClass : after.afters()) {
				try {
					AfterWorkerBase<byte[], ResponseMessageBase<byte[]>> afterWorker = (AfterWorkerBase<byte[], ResponseMessageBase<byte[]>>) afterWorkerClass.getConstructor().newInstance();
					for (int i = 0; i < afterWorkers.size(); i++) {
						AfterWorkerBase<byte[], ResponseMessageBase<byte[]>> current = afterWorkers.get(i);
						if (current.order() > afterWorker.order()) {
							afterWorkers.add(i, afterWorker);
							afterWorker = null;
							break;
						}
					}
					if (afterWorker != null) {
						afterWorkers.add(afterWorker);						
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
		}
		return afterWorkers;
	}

	public String getPath() {
		return path;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public Method getMethod() {
		return method;
	}

	public ParameterManager getParameterManager() {
		return parameterManager;
	}

	@SuppressWarnings("unchecked")
	public ResponseType act(Object... parameter) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		for (Interceptor<ContentType> interceptor : this.interceptors) {
			InterceptedResponse<ContentType> ir = interceptor.intercept(parameter);
			if (ir != null) {
				parameter = ir.getContent();
				if (ir.getAnswer() != null) {
					return (ResponseType) ir.getAnswer();
				}
			}
		}
		return (ResponseType) method.invoke(controller, parameter);
	}

	@SuppressWarnings("unchecked")
	public ResponseType before(HttpExchange http) {
		for (int i = 0; i < beforeWorkers.size(); i++) {
			ResponseType message = (ResponseType) beforeWorkers.get(i).before(http);
			if (message != null) {
				return message;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ResponseType after(ResponseType answer, HttpExchange http) {
		for (int i = 0; i < afterWorkers.size(); i++) {
			ResponseType message = (ResponseType) afterWorkers.get(i).after(answer, http);
			if (message != null) {
				return message;
			}
		}
		return (ResponseType) answer;
	}

	public boolean acceptsRawData() {
		return this.acceptsRawData;
	}
}
