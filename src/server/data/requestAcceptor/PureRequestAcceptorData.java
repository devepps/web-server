package server.data.requestAcceptor;

import java.lang.reflect.Method;

import json.JSONReader;
import server.data.responseMessage.PureResponseMessage;
import server.data.streams.TransferStream;
import server.data.type.RequestType;
import server.logger.ServerLogger;

public class PureRequestAcceptorData extends RequestAcceptorBaseData<TransferStream, PureResponseMessage> {

	public PureRequestAcceptorData(JSONReader jsonReader, String path, RequestType requestType, Method method) {
		super(jsonReader, path, requestType, method, method.getDeclaredAnnotation(PureRequestAcceptor.class).acceptsRawData());
	}

	@Override
	protected void initialize() {
		ServerLogger.info("new Pure REST: (path=\"" + getPath() + "\", method=\"" + getRequestType() + "\")");
	}
}
