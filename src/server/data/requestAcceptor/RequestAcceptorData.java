package server.data.requestAcceptor;

import java.lang.reflect.Method;

import json.JSONReader;
import server.data.responseMessage.ResponseMessage;
import server.data.type.RequestType;
import server.logger.ServerLogger;

public class RequestAcceptorData extends RequestAcceptorBaseData<Object[], ResponseMessage> {

	public RequestAcceptorData(JSONReader jsonReader, String path, RequestType requestType, Method method) {
		super(jsonReader, path, requestType, method, method.getDeclaredAnnotation(RequestAcceptor.class).acceptsRawData());
	}

	@Override
	protected void initialize() {
		ServerLogger.info("new REST: (path=\"" + getPath() + "\", method=\"" + getRequestType() + "\")");
	}
}
