package server.data.requestAcceptor;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import server.data.type.RequestType;

@Retention(RUNTIME)
@Target(METHOD)
public @interface PureRequestAcceptor {

	public String path();
	public RequestType requestType();
	public boolean acceptsRawData() default false;

}
