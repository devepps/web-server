package server.data.exceptions;

import data.exceptions.CustomException;

public class ControllerCreationException extends CustomException {

	private static final long serialVersionUID = 1L;

	public ControllerCreationException(Class<?> controllerClass) {
		super("An error occurred while reading controller class : " + controllerClass);
	}

	public ControllerCreationException(Class<?> controllerClass, Throwable e) {
		super(e, "An error occurred while reading controller class : " + controllerClass);
	}

}
