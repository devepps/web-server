package server.data.exceptions;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import data.exceptions.CustomException;

public class RequestAcceptorCreationException extends CustomException {

	private static final long serialVersionUID = 1L;

	public RequestAcceptorCreationException(Method method) {
		super("An error occurred while reading request acceptor : " + method);
	}

	public RequestAcceptorCreationException(Throwable e, Method method) {
		super(e, "An error occurred while reading request acceptor : " + method);
	}

	public RequestAcceptorCreationException(Method method, Parameter parameter) {
		super("An error occurred while reading request acceptor : " + method + " in handling parameter : " + parameter);
	}

	public RequestAcceptorCreationException(Throwable e, Method method, Parameter parameter) {
		super(e, "An error occurred while reading request acceptor : " + method + " in handling parameter : " + parameter);
	}

}
