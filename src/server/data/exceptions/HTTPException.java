package server.data.exceptions;

import server.data.type.ResponseType;

public class HTTPException extends RuntimeException {

	private static final long serialVersionUID = 7359261099590984842L;

	private ResponseType responseType;
	private String message;

	public HTTPException(ResponseType responseType, String message) {
		this.responseType = responseType;
		this.message = message;
	}

	public HTTPException(ResponseType responseType, Throwable e) {
		this.responseType = responseType;
		this.message = "An error occured: " + e.getMessage();
	}

	public ResponseType getResponseType() {
		return responseType;
	}

	public String getErrorMessage() {
		return message;
	}

}
