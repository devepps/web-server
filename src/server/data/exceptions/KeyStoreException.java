package server.data.exceptions;

import data.exceptions.CustomException;

public class KeyStoreException extends CustomException {

	private static final long serialVersionUID = 1L;

	public KeyStoreException() {
		super("An error occurred while managing the keystore.");
	}

	public KeyStoreException(Throwable e) {
		super(e, "An error occurred while managing the keystore.");
	}

}
