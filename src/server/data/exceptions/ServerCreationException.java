package server.data.exceptions;

import data.exceptions.CustomException;

public class ServerCreationException extends CustomException {

	private static final long serialVersionUID = 1L;

	public ServerCreationException() {
		super("The server failed to be created.");
	}

	public ServerCreationException(Throwable e) {
		super(e, "The server failed to be created.");
	}

}
