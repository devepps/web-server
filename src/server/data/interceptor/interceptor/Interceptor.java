package server.data.interceptor.interceptor;

public interface Interceptor<ResponseContentType> {

	public InterceptedResponse<ResponseContentType> intercept(Object[] parameter);

}
