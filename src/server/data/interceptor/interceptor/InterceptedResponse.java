package server.data.interceptor.interceptor;

import java.util.Arrays;

import server.data.responseMessage.ResponseMessageBase;

public class InterceptedResponse<ResponseContentType> {

	private Object[] content;
	private ResponseMessageBase<ResponseContentType> answer;

	public InterceptedResponse(Object[] content, ResponseMessageBase<ResponseContentType> answer) {
		super();
		this.content = content;
		this.answer = answer;
	}

	public Object[] getContent() {
		return content;
	}

	public ResponseMessageBase<ResponseContentType> getAnswer() {
		return answer;
	}

	@Override
	public String toString() {
		return "InterceptedResponse [content=" + Arrays.toString(content) + ", answer=" + answer + "]";
	}

}
