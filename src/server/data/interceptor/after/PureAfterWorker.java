package server.data.interceptor.after;

import server.data.responseMessage.PureResponseMessage;
import server.data.streams.TransferStream;

public interface PureAfterWorker extends AfterWorkerBase<TransferStream, PureResponseMessage> {

}
