package server.data.interceptor.after;

import com.sun.net.httpserver.HttpExchange;

import server.data.responseMessage.ResponseMessageBase;

public interface AfterWorkerBase <ContentType, ResponseType extends ResponseMessageBase<ContentType>> {

	public ResponseType after(ResponseType answer, HttpExchange http);
	
	public int order();

}
