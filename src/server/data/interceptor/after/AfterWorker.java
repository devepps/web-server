package server.data.interceptor.after;

import server.data.responseMessage.ResponseMessage;

public interface AfterWorker extends AfterWorkerBase<Object[], ResponseMessage> {

}
