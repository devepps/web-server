package server.data.interceptor.before;

import server.data.responseMessage.ResponseMessage;

public interface BeforeWorker extends BeforeWorkerBase<Object[], ResponseMessage> {

}
