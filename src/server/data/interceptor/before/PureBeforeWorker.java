package server.data.interceptor.before;

import server.data.responseMessage.PureResponseMessage;
import server.data.streams.TransferStream;

public interface PureBeforeWorker extends BeforeWorkerBase<TransferStream, PureResponseMessage> {

}
