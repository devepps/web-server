package server.data.interceptor.before;

import com.sun.net.httpserver.HttpExchange;

import server.data.responseMessage.ResponseMessageBase;

public interface BeforeWorkerBase <ContentType, ResponseType extends ResponseMessageBase<ContentType>> {

	public ResponseType before(HttpExchange http);

	public int order();

}
