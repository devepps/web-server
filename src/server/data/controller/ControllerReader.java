package server.data.controller;

import java.lang.reflect.Method;

import server.data.exceptions.ControllerCreationException;
import server.data.exceptions.RequestAcceptorCreationException;
import server.data.requestAcceptor.PureRequestAcceptor;
import server.data.requestAcceptor.PureRequestAcceptorData;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.requestAcceptor.RequestAcceptorData;
import server.logger.ServerLogger;
import server.serverManager.ServerManager;

public class ControllerReader {

	private ServerManager serverManager;

	public ControllerReader(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	public void readController(Class<?> controllerClass) {
		Controller controller = controllerClass.getDeclaredAnnotation(Controller.class);
		if (controller == null) {
			throw new ControllerCreationException(controllerClass);
		}
		long start = System.currentTimeMillis();
		ServerLogger.info("Start loading controller : " + controllerClass.getSimpleName());

		for (Method method : controllerClass.getDeclaredMethods()) {
			RequestAcceptor acceptor = method.getDeclaredAnnotation(RequestAcceptor.class);
			PureRequestAcceptor pureAcceptor = method.getDeclaredAnnotation(PureRequestAcceptor.class);

			if ((acceptor == null && pureAcceptor == null) || (acceptor != null && pureAcceptor != null)) {
				throw new RequestAcceptorCreationException(method);
			}

			if (acceptor != null) {
				serverManager.addRequestAcceptor(new RequestAcceptorData(serverManager.getJSONReader(),
						(controller.basePath().trim().indexOf('/') == 0 ? "" : "/") + controller.basePath().trim()
								+ (acceptor.path().trim().length() == 0 ? "" : "/") + acceptor.path().trim(),
						acceptor.requestType(), method));
			} else if (pureAcceptor != null) {
				serverManager.addRequestAcceptor(new PureRequestAcceptorData(serverManager.getJSONReader(),
						(controller.basePath().trim().indexOf('/') == 0 ? "" : "/") + controller.basePath().trim()
								+ (pureAcceptor.path().trim().length() == 0 ? "" : "/") + pureAcceptor.path().trim(),
						pureAcceptor.requestType(), method));
			}
		}
		ServerLogger.info("Controller " + controllerClass.getSimpleName() + " was loaded ("
				+ (System.currentTimeMillis() - start) + " ms)");
	}

}
