package server.data.parameter;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import com.sun.net.httpserver.Headers;

import collection.sync.SyncHashMap;
import exceptions.JSONException;
import json.JSONBaseType;
import json.JSONReader;
import server.data.exceptions.RequestAcceptorCreationException;
import server.logger.ServerLogger;

public class ParameterManager {

	private final SyncHashMap<String, ParameterData> bodyParameters = new SyncHashMap<>();
	private final SyncHashMap<String, ParameterData> urlParameters = new SyncHashMap<>();

	public ParameterManager(JSONReader jsonReader, Method method) {
		Parameter[] parameters = method.getParameters();
		for (int i = 0; i < parameters.length; i++) {
			Parameter parameter = parameters[i];
			ParameterConfig config = parameter.getDeclaredAnnotation(ParameterConfig.class);
			if (config == null) {
				throw new RequestAcceptorCreationException(method, parameter);
			}
			ParameterData parameterData = null;
				switch (config.contentType()) {
				case BASE_TYPE:
					parameterData = new ParameterRawData(i, parameter.getType(), config.name());
					break;
				case JSON_OBJECT:
					parameterData = new ParameterObjectData(i, jsonReader.createJSONType(parameter.getType()), config.name());
					break;
			}
			switch (config.parameterType()) {
				case BODY:
					this.bodyParameters.put(parameterData.getName(), parameterData);
					break;
				case URL:
					this.urlParameters.put(parameterData.getName(), parameterData);
					break;
			}
		}
	}

	public Object[] load(Headers requestHeaders, InputStream requestBody, String url, boolean acceptsRawData) throws IOException {
		Object[] data = new Object[bodyParameters.size() + urlParameters.size()];
		
		ServerLogger.debug("incoming-url: " + url);
		// <=== get URL-Arguments ==>
		url = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
		int startArgs = url.indexOf('?');
		String[] arguments = url.substring(startArgs + 1).split(Pattern.quote("&"));
		ServerLogger.debug("incoming-url refactored to: " + Arrays.toString(arguments));

		// <=== parse URL-Arguments ==>
		for (String argument : arguments) {
			int startContent = argument.indexOf('=');		
			if(startContent != -1) {
				String name = argument.substring(0, startContent);
				ParameterData parameterData = urlParameters.get(name);
				ServerLogger.debug("url-paramter: " + name + " -> " + parameterData);
				if (parameterData != null) {
					try {
						String json = "{\"" + name + "\" : "
								+ (parameterData.getContentClass().isAssignableFrom(String.class) ? "\"" : "")
								+ argument.substring(startContent + 1)
								+ (parameterData.getContentClass().isAssignableFrom(String.class) ? "\"" : "") + "}";
						ServerLogger.debug("url-paramter: " + name + " -> " + json);
						data[parameterData.getId()] = getParameter(parameterData, (JsonObject) Jsoner.deserialize(json), name);
					} catch (JsonException e) {
						e.printStackTrace();
					}
				}	
			}
		}

		// <=== get Request-Body ===>
		if (!requestHeaders.containsKey("Content-Length")) {
			return data;
		}
		int contentLength = Integer.parseInt(requestHeaders.get("Content-Length").get(0));

		if (!acceptsRawData) {
			// <=== parse Request-Body to JSON ===>
			byte[] incomeData = new byte[contentLength];
			new DataInputStream(requestBody).readFully(incomeData);

			JsonObject jsonObject = null;
			try {
				String jason = new String(incomeData, 0, contentLength, StandardCharsets.UTF_8);
				jsonObject = (JsonObject) Jsoner.deserialize(jason);
			} catch (JsonException e) {
				e.printStackTrace();
				throw new JSONException(new String(incomeData, 0, contentLength));
			}
			// <=== read Request-Body JSON ===>
			for (String key : jsonObject.keySet()) {
				ParameterData parameterData = bodyParameters.get(key);
				if (parameterData != null) {
					data[parameterData.getId()] = getParameter(parameterData, jsonObject, key);
				}
			}
		} else {
			// <=== read raw data ===>
			ParameterData parameterData = (ParameterData) bodyParameters.values().toArray()[0];
			data[parameterData.getId()] = requestBody;
		}
		return data;
	}

	private Object getParameter(ParameterData parameterData, JsonObject jsonObject, String key) {
		if (parameterData instanceof ParameterObjectData) {
			ParameterObjectData pod = (ParameterObjectData) parameterData;
			try {
				Object result = pod.getJsonType().decode((JsonObject) jsonObject.get(key));
				return result;
			} catch (IllegalArgumentException | IllegalAccessException | ClassCastException
					| InstantiationException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
				ServerLogger.debug("Failed to read " + key + ".", e);
			}
		} else if (parameterData instanceof ParameterRawData) {
			ParameterRawData prd = (ParameterRawData) parameterData;
			JSONBaseType baseType = JSONBaseType.getBaseTypes(prd.getContentClass());
			if (baseType == null) {
				ServerLogger.debug("Failed to read " + key + ".");						
			} else {
				return baseType.getDecoded(jsonObject.get(key));						
			}
		}
		return null;
	}

}
