package server.data.parameter;

public class ParameterRawData extends ParameterData {

	public ParameterRawData(int id, Class<?> contentClass, String name) {
		super(id, contentClass, name);
	}

	@Override
	public String toString() {
		return "ParameterRawData [getId()=" + getId() + ", getContentClass()=" + getContentClass() + ", getName()="
				+ getName() + "]";
	}

}
