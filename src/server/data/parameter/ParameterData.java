package server.data.parameter;

public abstract class ParameterData {

	private int id;
	private Class<?> contentClass;
	private String name;

	public ParameterData(int id, Class<?> contentClass, String name) {
		super();
		this.id = id;
		this.contentClass = contentClass;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public Class<?> getContentClass() {
		return contentClass;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "ParameterData [id=" + id + ", contentClass=" + contentClass + ", name=" + name + "]";
	}

}
