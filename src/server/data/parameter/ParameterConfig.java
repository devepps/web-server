package server.data.parameter;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import server.data.type.ContentType;
import server.data.type.ParameterType;

@Retention(RUNTIME)
@Target(PARAMETER)
public @interface ParameterConfig {

	ContentType contentType() default ContentType.BASE_TYPE;

	ParameterType parameterType() default ParameterType.BODY;
	
	String name();

}
