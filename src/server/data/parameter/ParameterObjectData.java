package server.data.parameter;

import json.JSONType;

public class ParameterObjectData extends ParameterData {

	private JSONType jsonType;

	public ParameterObjectData(int id, JSONType jsonType, String name) {
		super(id, jsonType.getSuperClass(), name);
		this.jsonType = jsonType;
	}

	public JSONType getJsonType() {
		return jsonType;
	}

	@Override
	public String toString() {
		return "ParameterObjectData [jsonType=" + jsonType + ", getId()=" + getId() + ", getContentClass()="
				+ getContentClass() + ", getName()=" + getName() + "]";
	}

}
