package server.data.streams;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class TransferStream {

	private InputStream in;
	private long contentLength;

	public TransferStream(InputStream in, long contentLength) {
		super();
		this.in = in;
		this.contentLength = contentLength;
	}

	public TransferStream(byte[] data) {
		super();
		this.in = new ByteArrayInputStream(data);
		this.contentLength = data.length;
	}

	public InputStream getStream() {
		return in;
	}

	public long getContentLength() {
		return contentLength;
	}

}
