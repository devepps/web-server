package server.data.internalDTO;

import json.JSON;

@JSON(name = "exceptionResponse")
public class ErrorDTO {

	private String message;

	public ErrorDTO(String message) {
		super();
		this.message = message;
	}

	public ErrorDTO() {
		super();
	}

	@Override
	public String toString() {
		return "ExceptionResponse [message=" + message + "]";
	}
}
