package server.data.type;

public enum RequestType {

	GET(), PUT(), POST(), DELETE(), NONE();

	public static RequestType getRequestType(String name) {
		for (RequestType requestType : values()) {
			if (requestType.name().equalsIgnoreCase(name.trim())) {
				return requestType;
			}
		}
		return NONE;
	}
}
