package server.data.responseMessage;

import server.data.type.ResponseType;

public abstract class ResponseMessageBase <ContentType> {

	private ResponseType responseStatus;
	private ContentType content;

	public ResponseMessageBase(ResponseType responseStatus, ContentType content) {
		this.responseStatus = responseStatus;
		this.content = content;
	}
	
	public abstract String getContentType();

	public ResponseType getResponseStatus() {
		return responseStatus;
	}

	public ContentType getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "ResponseMessageBase [responseStatus=" + responseStatus + ", content=" + content + "]";
	}

}
