package server.data.responseMessage;

import server.data.streams.TransferStream;
import server.data.type.ResponseType;

public class PureResponseMessage extends ResponseMessageBase<TransferStream> {

	private String contentType;

	public PureResponseMessage(ResponseType responseStatus, TransferStream content, String contentType) {
		super(responseStatus, content);
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		return "PureResponseMessage [responseStatus=" + getResponseStatus() + ", content=" + getContent() + "]";
	}

	@Override
	public String getContentType() {
		return contentType;
	}

}
