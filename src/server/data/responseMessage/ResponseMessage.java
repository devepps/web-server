package server.data.responseMessage;

import java.util.Arrays;

import server.data.type.ResponseType;

public class ResponseMessage extends ResponseMessageBase<Object[]> {

	public ResponseMessage(ResponseType responseStatus, Object... content) {
		super(responseStatus, content);
	}

	@Override
	public String toString() {
		return "ResponseMessage [responseStatus=" + getResponseStatus() + ", content=" + Arrays.toString(getContent()) + "]";
	}

	@Override
	public String getContentType() {
		return "application/json";
	}

}
