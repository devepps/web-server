package server.serverManager;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import config.ConfigEnvironment;
import json.JSONReader;
import server.data.exceptions.HTTPException;
import server.data.internalDTO.ErrorDTO;
import server.data.requestAcceptor.RequestAcceptorBaseData;
import server.data.responseMessage.PureResponseMessage;
import server.data.responseMessage.ResponseMessage;
import server.data.responseMessage.ResponseMessageBase;
import server.data.type.RequestType;
import server.data.type.ResponseType;
import server.logger.ServerLogger;

public class ServerManager {
	
	public static final ServerService SERVER_SERVICE = new ServerService();
	private static final String PROXY_HEADER_KEY_IP = ConfigEnvironment.getProperty("proxy.header-key.ip");
	private static final boolean PROXY_ENABLED = "true".equalsIgnoreCase(ConfigEnvironment.getProperty("proxy.enabled"));

	private final HashMap<String, ArrayList<RequestAcceptorBaseData<?, ? extends ResponseMessageBase<?>>>> acceptors = new HashMap<>();
	private final JSONReader jsonReader = new JSONReader();

	private HTTPErrorHandler httpErrorHandler = new DefaultHTTPErrorHandler();
	private Server server;

	public ServerManager(Server server) {
		super();
		this.server = server;
	}

	public void addRequestAcceptor(RequestAcceptorBaseData<?, ? extends ResponseMessageBase<?>> requestAcceptorData) {
		ArrayList<RequestAcceptorBaseData<?, ? extends ResponseMessageBase<?>>> acceptors = this.acceptors.get(requestAcceptorData.getPath());
		if (acceptors == null) {
			acceptors = new ArrayList<>();
			this.acceptors.put(requestAcceptorData.getPath(), acceptors);
		}
		acceptors.add(requestAcceptorData);
	}

	public void setHttpErrorHandler(HTTPErrorHandler httpErrorHandler) {
		this.httpErrorHandler = httpErrorHandler;
	}

	public void build() {
		for (String path : acceptors.keySet()) {
			server.addContext(path, new HttpHandler() {
				ArrayList<RequestAcceptorBaseData<?, ? extends ResponseMessageBase<?>>> pathAcceptors = acceptors.get(path);
				@SuppressWarnings("unchecked")
				@Override
				public void handle(HttpExchange http) throws IOException {
					if (server.handleCores(http)) {
						return;
					}
					long start = System.currentTimeMillis();
					ServerLogger.info("New https request: " + path + " -> " + http.getRequestMethod());

					ResponseMessageBase<?> answer = null;
					try {
						for (RequestAcceptorBaseData<?, ? extends ResponseMessageBase<?>> acceptor : pathAcceptors) {
							String ip = http.getRemoteAddress().getAddress().getHostAddress();
							if (PROXY_ENABLED && http.getRequestHeaders().containsKey(PROXY_HEADER_KEY_IP)) {
								InetAddress address = InetAddress.getByName(http.getRequestHeaders().get(PROXY_HEADER_KEY_IP).get(0));
								ip = address.getHostName();
							}
							SERVER_SERVICE.registerConection(ip);
							if (answer == null) {
								answer = act((RequestAcceptorBaseData<?, ResponseMessageBase<?>>) acceptor, http);
							}
							SERVER_SERVICE.removeConection();
							if(answer != null)
								break;
						}
					} catch (Throwable e) {
						answer = httpErrorHandler.handleError(e);
						if (!(e.getCause() instanceof HTTPException)) {
							e.printStackTrace();
						}
					}
					
					if (answer == null) {
						answer = new ResponseMessage(ResponseType.METHOD_NOT_ALLOWED,
								new ErrorDTO("Request method is not allowed."));
					}
					try {
						if (answer instanceof ResponseMessage) {
							ResponseMessage responseAnswer = (ResponseMessage) answer;
							byte[] answerData = jsonReader.readJSONObject(responseAnswer.getContent());
							server.answer(http, answer.getResponseStatus(), answerData, answer.getContentType());
						} else if (answer instanceof PureResponseMessage) {
							PureResponseMessage responseAnswer = (PureResponseMessage) answer;
							server.answer(http, answer.getResponseStatus(), responseAnswer.getContent(), answer.getContentType());
						}
					} catch (Throwable e) {
						e.printStackTrace();
					}
					ServerLogger.info("Handled https request in " + (System.currentTimeMillis() - start) + " ms.");					
				}

				private ResponseMessageBase<?> act(RequestAcceptorBaseData<?, ResponseMessageBase<?>> acceptor, HttpExchange http) throws IOException {
					if (RequestType.getRequestType(http.getRequestMethod()) != acceptor.getRequestType()) {
						return null;
					}

					ServerLogger.debug("request accepted");
					ResponseMessageBase<?> answer = null;
					answer = acceptor.before(http);

					if (answer == null) {
						try {
							answer = acceptor.act(
									acceptor.getParameterManager().load(http.getRequestHeaders(), http.getRequestBody(),
											new String(http.getRequestURI().toASCIIString().getBytes(), StandardCharsets.ISO_8859_1), acceptor.acceptsRawData()));
						} catch (Throwable e) {
							answer = httpErrorHandler.handleError(e);
							if (!(e.getCause() instanceof HTTPException)) {
								e.printStackTrace();
							}
						}
						if (answer == null) {
							answer = new ResponseMessage(ResponseType.INTERNAL_SERVER_ERROR,
									new ErrorDTO("An unexpected error occurred."));
						}
					}
					answer = acceptor.after(answer, http);
					return answer;
				}
			});
		}
	}

	public void addJSONType(Class<?> jsonClass) {
		long start = System.currentTimeMillis();
		ServerLogger.info("Start loading JSONType : " + jsonClass.getSimpleName());
		this.jsonReader.createJSONType(jsonClass);
		ServerLogger.info("JSONType " + jsonClass.getSimpleName() + " was loaded (" + (System.currentTimeMillis() - start) + " ms)");
	}

	public JSONReader getJSONReader() {
		return jsonReader;
	}

}
