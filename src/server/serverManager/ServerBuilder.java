package server.serverManager;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import server.data.controller.ControllerReader;
import server.data.exceptions.ServerCreationException;
import server.data.internalDTO.ErrorDTO;

public class ServerBuilder {

	private Server server;
	private ServerManager serverManager;
	private ControllerReader controllerReader;

	public ServerBuilder(String configPort, String configPathToFile, String configFilePassword) {
		try {
			this.server = new Server(configPort, configPathToFile, configFilePassword);
		} catch (UnrecoverableKeyException e) {
			throw new server.data.exceptions.KeyStoreException(e);
		} catch (KeyManagementException e) {
			throw new server.data.exceptions.KeyStoreException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new ServerCreationException(e);
		} catch (CertificateException e) {
			throw new ServerCreationException(e);
		} catch (KeyStoreException e) {
			throw new server.data.exceptions.KeyStoreException(e);
		} catch (IOException e) {
			throw new ServerCreationException(e);
		}
		this.serverManager = new ServerManager(server);
		this.controllerReader = new ControllerReader(serverManager);
	}

	public ServerBuilder addController(Class<?> controllerClass) {
		this.controllerReader.readController(controllerClass);
		return this;
	}

	public ServerBuilder addJSONType(Class<?> jsonClass) {
		this.serverManager.addJSONType(jsonClass);
		return this;
	}

	public ServerBuilder setErrorHandler(HTTPErrorHandler httpErrorHandler) {
		this.serverManager.setHttpErrorHandler(httpErrorHandler);
		return this;
	}
	
	public void build() {
		this.serverManager.addJSONType(ErrorDTO.class);
		this.serverManager.build();
		this.server.start();
	}

}
