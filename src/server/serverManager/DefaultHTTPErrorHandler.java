package server.serverManager;

import server.data.exceptions.HTTPException;
import server.data.internalDTO.ErrorDTO;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ResponseType;

public class DefaultHTTPErrorHandler implements HTTPErrorHandler {

	@Override
	public ResponseMessage handleError(Throwable e) {
		if (e.getCause() != null) {
			return handleError(e.getCause());
		}
		if (e instanceof HTTPException) {
			HTTPException httpException = (HTTPException) e;
			return new ResponseMessage(httpException.getResponseType(), new ErrorDTO(httpException.getErrorMessage()));
		}
		return new ResponseMessage(ResponseType.INTERNAL_SERVER_ERROR, new ErrorDTO(e.getMessage()));
	}

}
