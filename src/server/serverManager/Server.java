package server.serverManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import config.ConfigEnvironment;
import server.data.streams.TransferStream;
import server.data.type.ResponseType;
import server.logger.ServerLogger;

public class Server {

	private HttpsServer httpsServer;

	public Server(String configPort, String configPathToFile, String configFilePassword)
			throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException,
			UnrecoverableKeyException, KeyManagementException {

		this.httpsServer = HttpsServer.create(new InetSocketAddress(ConfigEnvironment.getPropertyInt(configPort)), 10);

		String password = ConfigEnvironment.getProperty(configFilePassword);
		SSLContext sslContext = SSLContext.getInstance("TLS");

		KeyStore ks = KeyStore.getInstance("JKS");
		FileInputStream fis = new FileInputStream(ConfigEnvironment.getProperty(configPathToFile));
		ks.load(fis, password.toCharArray());

		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, password.toCharArray());

		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(ks);

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
		httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
			public void configure(HttpsParameters params) {
				try {
					// Initialise the SSL context
					SSLContext c = SSLContext.getDefault();
					SSLEngine engine = c.createSSLEngine();
					params.setNeedClientAuth(false);
					params.setCipherSuites(engine.getEnabledCipherSuites());
					params.setProtocols(engine.getEnabledProtocols());

					// Get the default parameters
					SSLParameters defaultSSLParameters = c.getDefaultSSLParameters();
					params.setSSLParameters(defaultSSLParameters);
				} catch (Exception e) {
					ServerLogger.debug("Failed to create HTTPS port", e);
				}
			}
		});
	}

	public void addContext(String path, HttpHandler httpHandler) {
		this.httpsServer.createContext(path, httpHandler);
	}

	public void start() {
		httpsServer.setExecutor(Executors.newCachedThreadPool());
		httpsServer.start();
		ServerLogger.info("Server started");
	}

	public void answer(HttpExchange http, ResponseType status, byte[] content, String contentType) {
		http.getResponseHeaders().add("Content-Type", contentType);
		http.getResponseHeaders().add("Connection:", "Keep-Alive");
		http.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST, DELETE");
		http.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		http.getResponseHeaders().add("Access-Control-Allow-Headers", "*");
		try {
			http.sendResponseHeaders(status.getStatusCode(), content.length);
			http.getResponseBody().write(content);
			http.getResponseBody().close();
		} catch (Throwable e) {
			byte[] errorMessage = "An unexpected error occurred.".getBytes();
			try {
				http.sendResponseHeaders(ResponseType.INTERNAL_SERVER_ERROR.getStatusCode(), errorMessage.length);
				http.getResponseBody().write(errorMessage);
				http.getResponseBody().close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public boolean handleCores(HttpExchange http) {
		ServerLogger.info("Start answering Options: " + http.getRequestMethod());
		if (http.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			http.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST, DELETE");
			http.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			http.getResponseHeaders().add("Access-Control-Allow-Headers", "*");
			try {
				http.sendResponseHeaders(ResponseType.ACCEPTED.getStatusCode(), 0);
				http.getResponseBody().close();
				ServerLogger.info("Answered options: " + http.getRequestMethod());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	public void answer(HttpExchange http, ResponseType status, TransferStream content, String contentType) {
		http.getResponseHeaders().add("Content-Type", contentType);
		http.getResponseHeaders().add("Connection:", "Keep-Alive");
		http.getResponseHeaders().add("Access-Control-Allow-Methods","GET, OPTIONS, PUT, POST, DELETE");
		http.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		http.getResponseHeaders().add("Access-Control-Allow-Headers", "*");
		try {
			http.sendResponseHeaders(status.getStatusCode(), content.getContentLength());
			byte[] buffer = new byte[1024 * 1024];
		    for (int bytesRead = content.getStream().read(buffer); bytesRead != -1; bytesRead = content.getStream().read(buffer)) {
		    	http.getResponseBody().write(buffer, 0, bytesRead);
		    }
			http.getResponseBody().close();
		} catch (Throwable e) {
			byte[] errorMessage = "An unexpected error occurred.".getBytes();
			try {
				http.sendResponseHeaders(ResponseType.INTERNAL_SERVER_ERROR.getStatusCode(), errorMessage.length);
				http.getResponseBody().write(errorMessage);
				http.getResponseBody().close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			e.printStackTrace();
		}
	}

}
