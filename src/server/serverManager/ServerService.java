package server.serverManager;

import collection.sync.SyncHashMap;
import collection.sync.SyncQueue;
import data.queue.Action.ActionType;
import server.data.interceptor.AfterService;
import server.data.interceptor.BeforeService;

public class ServerService {

	private final SyncHashMap<Thread, String> connectionInformation = new SyncHashMap<>();
	private final SyncQueue<AfterService> afterServices = new SyncQueue<>();
	private final SyncQueue<BeforeService> beforeServices = new SyncQueue<>();

	public void registerConection(String ip) {
		if (ip.contains(":")) {
			ip = ip.substring(0, ip.indexOf(":"));
		}
		this.connectionInformation.put(Thread.currentThread(), ip);
		this.beforeServices.actOnContent((BeforeService service) -> {
			service.connectionOpen();
			return ActionType.NONE;
		});
	}

	public void removeConection() {
		this.afterServices.actOnContent((AfterService service) -> {
			service.connectionClose();
			return ActionType.NONE;
		});
		this.connectionInformation.remove(Thread.currentThread());
	}

	public String getIP() {
		return this.connectionInformation.get(Thread.currentThread());
	}

	public void registerAfterService(AfterService afterService) {
		this.afterServices.add(afterService);
	}

	public void registerBeforeService(BeforeService beforeService) {
		this.beforeServices.add(beforeService);
	}
}