package server.serverManager;

import server.data.responseMessage.ResponseMessage;

public interface HTTPErrorHandler {
	
	public ResponseMessage handleError(Throwable e);

}
