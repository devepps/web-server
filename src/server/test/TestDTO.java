package server.test;
import java.util.Arrays;

import json.JSON;

@JSON(name = "test")
public class TestDTO {

	private Integer integerType;
	private Float floatType;
	private String stringType;
	private Integer[] dataList = new Integer[] { 17, 9, 13, 12 };
	private Float[] dataArray = new Float[] { 2.3f, 4.6f, 7.9f };
	private TestDTO2 testDTO = new TestDTO2(44);
	private TestDTO2[] dataArray2 = new TestDTO2[] { new TestDTO2(35), new TestDTO2(27), new TestDTO2(13) };

	public TestDTO() {
		super();
	}

	public TestDTO(Integer integerType, Float floatType, String stringType) {
		super();
		this.integerType = integerType;
		this.floatType = floatType;
		this.stringType = stringType;
	}

	@Override
	public String toString() {
		return "TestDTO [integerType=" + integerType + ", floatType=" + floatType + ", stringType=" + stringType
				+ ", dataList=" + dataList + ", dataArray=" + Arrays.toString(dataArray) + ", testDTO=" + testDTO
				+ ", dataArray2=" + Arrays.toString(dataArray2) + "]";
	}
}
