package server.test;
import server.data.controller.Controller;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller
public class TestController {

	public TestController() {
	}

	@RequestAcceptor(path = "test", requestType = RequestType.GET)
	private ResponseMessage test() {
		return new ResponseMessage(ResponseType.OK, new TestDTO(99, 3.2f, "test Text"));
	}

	@RequestAcceptor(path = "test", requestType = RequestType.POST)
	private ResponseMessage test(@ParameterConfig(contentType = ContentType.JSON_OBJECT, name = "testDTO") TestDTO test) {
		return new ResponseMessage(ResponseType.OK, new TestDTO2(12), test);
	}

	@RequestAcceptor(path = "test2", requestType = RequestType.GET)
	private ResponseMessage test2(@ParameterConfig(parameterType = ParameterType.URL, name = "name") String name) {
		return new ResponseMessage(ResponseType.OK, new TestDTO(99, 3.2f, name));
	}
}
