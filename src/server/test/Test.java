package server.test;
import server.serverManager.ServerBuilder;

public class Test {

	public static void main(String args[]) {
		new Test();
	}

	public Test() {
		new ServerBuilder("https.server.port", "https.keyStore.path", "https.keyStore.password")
				.addJSONType(TestDTO.class).addJSONType(TestDTO2.class).addController(TestController.class).build();
	}

}
