package server.test;
import json.JSON;

@JSON(name = "test2")
public class TestDTO2 {

	private Integer integerType2;
	private Integer[] dataList2 = null;

	public TestDTO2() {
		super();
	}

	public TestDTO2(Integer integerType2) {
		super();
		this.integerType2 = integerType2;
	}

	@Override
	public String toString() {
		return "TestDTO2 [integerType2=" + integerType2 + ", dataList2=" + dataList2 + "]";
	}

}
