package json;

import java.nio.charset.StandardCharsets;

import com.github.cliftonlabs.json_simple.JsonArray;

import collection.sync.SyncHashMap;
import exceptions.JSONTypeCreationException;
import exceptions.JSONTypeReadException;

public class JSONReader {

	private final SyncHashMap<String, JSONType> jsonTypes = new SyncHashMap<>();
	
	public JSONType createJSONType(Class<?> jsonClass) {
		JSON json = jsonClass.getDeclaredAnnotation(JSON.class);
		if (json == null) {
			throw new JSONTypeCreationException(jsonClass);
		}
		JSONType jsonType = jsonTypes.get(json.name());
		if(jsonType == null) {
			jsonType = new JSONType(jsonClass);
			jsonTypes.put(json.name(), jsonType);
		}
		return jsonType;
	}

	public byte[] readJSONObject(Object... objects) {
		JsonArray jsonObject = new JsonArray();
		for(Object obj : objects) {
			JSON json = obj.getClass().getDeclaredAnnotation(JSON.class);
			if(json == null) {
				JSONBaseType jsonBaseType = JSONBaseType.getBaseTypes(obj.getClass());
				if (jsonBaseType == null) {
					throw new JSONTypeReadException(obj);					
				} else {
					jsonObject.add(jsonBaseType.getEncoded(obj));
				}
			} else {
				JSONType type = jsonTypes.get(json.name());
				if(type == null) {
					throw new JSONTypeReadException(obj);
				}
				try {
					jsonObject.add(type.encode(obj));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
					throw new JSONTypeReadException(obj, e);
				}
			}
		}
		return jsonObject.toJson().trim().getBytes(StandardCharsets.ISO_8859_1);
	}
}
