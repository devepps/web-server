package json.jsoner;

import java.nio.charset.Charset;

public class StringByte {
	
	private final static Charset UNICODE = Charset.forName("Unicode");
	
	private byte[] data = new byte[0];

	public StringByte(byte[] data) {
		this.data = (data == null) ? new byte[0] : data;
	}

	public StringByte(String data) {
		this.data = data.getBytes(UNICODE);
	}
	
	public String getString() {
		return new String(data, UNICODE);
	}
	
	public byte[] getByte() {
		return data;
	}

	@Override
	public String toString() {
		return "StringByte [getString()=" + getString() + "]";
	}

}
