package json;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;

import exceptions.JSONTypeCreationException;
import exceptions.JSONTypeReadException;

public class JSONType {
	
	private static final HashMap<Class<?>, JSONType> TYPES = new HashMap<>();

	private Class<?> superClass;
	private String name;

	JSONType(Class<?> superClass) {
		this.superClass = superClass;
		JSON json = superClass.getDeclaredAnnotation(JSON.class);
		if (json == null) {
			throw new JSONTypeCreationException(superClass);
		}
		this.name = json.name();
		TYPES.put(superClass, this);
	}

	public String getName() {
		return name;
	}

	public Class<? extends Object> getSuperClass() {
		return superClass;
	}

	public JsonObject encode(Object superObject) throws IllegalArgumentException, IllegalAccessException {
		JsonObject jsonObject = new JsonObject();
		if (superObject == null) {
			return null;
		}
		for (Field field : superClass.getDeclaredFields()) {
			JsonIgnore jsonIgnore = field.getDeclaredAnnotation(JsonIgnore.class);
			if (jsonIgnore == null) {
				field.setAccessible(true);
				encodeType(field, jsonObject, superObject);
			}
		}
		return jsonObject;
	}

	private void encodeType(Field field, JsonObject jsonObject, Object superObject)
			throws IllegalArgumentException, IllegalAccessException {
		if (field.get(superObject) == null) {
			jsonObject.put(field.getName(), null);
			return;
		}
		JSONBaseType baseType = JSONBaseType.getBaseTypes(field.getType());
		if (baseType != null) {
			jsonObject.put(field.getName(), baseType.getEncoded(field.get(superObject)));
		} else {
			JSONType jsonType = TYPES.get(field.getType());
			if (jsonType != null) {
				jsonObject.put(field.getName(), jsonType.encode(field.get(superObject)));
			} else {
				if (field.getType().isArray()) {
					Object[] list = (Object[]) field.get(superObject);
					JsonArray jsonArray = new JsonArray();
					for (Object obj : list) {
						if (obj == null) {
							jsonArray.add(null);
						} else {
							encodeElement(jsonArray, obj);
						}
					}
					jsonObject.put(field.getName(), jsonArray);
				} else {
					throw new JSONTypeReadException(superObject);
				}
			}
		}
	}

	private void encodeElement(JsonArray jsonArray, Object obj) throws IllegalArgumentException, IllegalAccessException {
		if (obj == null) {
			jsonArray.add(null);
			return;
		}
		JSONBaseType baseType = JSONBaseType.getBaseTypes(obj.getClass());
		if (baseType != null) {
			jsonArray.add(baseType.getEncoded(obj));
		} else {
			JSONType jsonType = TYPES.get(obj.getClass());
			if (jsonType != null) {
				jsonArray.add(jsonType.encode(obj));
			} else {
				if (obj.getClass().isArray()) {
					Object[] list = (Object[]) obj;
					JsonArray subJsonArray = new JsonArray();
					for (Object subObj : list) {
						encodeElement(subJsonArray, subObj);
					}
					jsonArray.add(subJsonArray);
				} else {
					throw new JSONTypeReadException(obj);
				}
			}
		}
	}

	public Object decode(JsonObject jsonObject) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Object newInstance = superClass.getConstructor().newInstance();
		for (Field field : superClass.getDeclaredFields()) {
			JsonIgnore jsonIgnore = field.getDeclaredAnnotation(JsonIgnore.class);
			if (jsonIgnore == null) {
				field.setAccessible(true);
				decodeType(newInstance, field, jsonObject);
			}
		}
		return newInstance;
	}

	private void decodeType(Object newInstance, Field field, JsonObject jsonObject)
			throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, SecurityException {
		JSONBaseType baseType = JSONBaseType.getBaseTypes(field.getType());
		if (baseType != null) {
			field.set(newInstance, baseType.getDecoded(jsonObject.get(field.getName())));
		} else {
			JSONType jsonType = TYPES.get(field.getType());
			if (jsonType != null) {
				field.set(newInstance, jsonType.decode((JsonObject) jsonObject.get(field.getName())));
			} else {
				if (field.getType().isArray()) {
					ArrayList<Object> list = new ArrayList<>();
					JsonArray jsonArray = (JsonArray) jsonObject.get(field.getName());
					if (jsonArray != null) {
						for (Object obj : jsonArray) {
							decodeElement(obj, list, field.getType().getComponentType());
						}
						field.set(newInstance, list.toArray(
								(Object[]) Array.newInstance(field.getType().getComponentType(), list.size())));
					}
				}
			}
		}
	}

	private void decodeElement(Object obj, ArrayList<Object> list, Class<?> contentType)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if (obj == null) {
			list.add(null);
			return;
		}
		JSONBaseType baseType = JSONBaseType.getBaseTypes(contentType);
		if (baseType != null) {
			list.add(baseType.getDecoded(obj));
		} else {
			if (obj instanceof JsonObject) {
				JsonObject jsonObject = (JsonObject) obj;
				JSONType jsonType = TYPES.get(contentType);
				if (jsonType == null) {
					throw new JSONTypeReadException(jsonObject);
				}
				list.add(jsonType.decode(jsonObject));
			} else if (obj instanceof JsonArray) {
				ArrayList<Object> subList = new ArrayList<>();
				for (Object subObj : (JsonArray) obj) {
					decodeElement(subObj, subList, contentType.getComponentType());
				}
				list.add(subList.toArray(
						(Object[]) Array.newInstance(contentType.getComponentType(), list.size())));
			}
		}
	}
}
