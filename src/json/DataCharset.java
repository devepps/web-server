package json;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.IntStream;

public class DataCharset {

	public DataCharset() {
	}

	public String toString(byte[] object) {
		IntStream newStream = IntStream.range(0, object.length * 2).parallel().map(index -> mapToData(index, object[index]));
		byte[] newData = newStream.parallel().collect(ByteArrayOutputStream::new, (baos, i) -> baos.write((byte) i),
	            (baos1, baos2) -> baos1.write(baos2.toByteArray(), 0, baos2.size()))
	            .toByteArray();
		return new String(newData, StandardCharsets.ISO_8859_1);
	}

	public byte[] toBytes(String object) {
		byte[] in = object.getBytes(StandardCharsets.ISO_8859_1);
		byte[] byteData = new byte[(in.length / 2)];
		for (int i = 0; i < in.length / 2; i++) {
			byte startValue = in[i * 2];
			byteData[i] = (startValue != 0) ? mapFromData(startValue) : in[(i * 2) + 1];
		}
		for (int i = 0; i < byteData.length; i++) {
		}
		return byteData;
	}

	private byte mapFromData(byte v) {
	    if (v == 65) { // "
	        return 34;
	      } else if (v == 66) { // '
	         return 39;
	      } else if (v == 67) { // &
	         return 38;
	       }  else if (v == 68) { // /
	         return 47;
	      } else if (v == 69) { // :
	         return 58;
	      } else if (v == 70) { // \
	         return 92;
	      } else if (v == 71) { // >
	         return 60;
	      } else if (v == 72) { // <
	         return 62;
	      } else if (v >= 73 && v < 90) { // 
	         return (byte) (127 + v - 73);
	      } else if (v >= 98 && v <= 113) { // 
		         return (byte) (127 + v - 73 - 8);
	      } else {
	         return v;
	      }
	}

	public byte mapToData(int key, byte v) {
	    if (v == 34) { // "
	      return (byte) (key % 2 == 0 ? 65 : 0);
	    } else if (v == 39) { // '
	       return (byte) (key % 2 == 0 ? 66 : 0);
	    } else if (v == 38) { // &
	       return (byte) (key % 2 == 0 ? 67 : 0);
	     }  else if (v == 47) { // /
	       return (byte) (key % 2 == 0 ? 68 : 0);
	    } else if (v == 58) { // :
	       return (byte) (key % 2 == 0 ? 69 : 0);
	    } else if (v == 92) { // \
	       return (byte) (key % 2 == 0 ? 70 : 0);
	    } else if (v == 60) { // >
	       return (byte) (key % 2 == 0 ? 71 : 0);
	    } else if (v == 62) { // <
	       return (byte) (key % 2 == 0 ? 72 : 0);
	    } else if (v >= 127 && v <= 159) { // \n
	       byte newValue = (byte) (73 + v - 127);
	       return (byte) (key % 2 == 0 ? (newValue >= 90 ? newValue + 8 : newValue) : 0);
	    } else {
	       return (byte) (key % 2 == 1 ? v : 0);
	    }
	}
}
