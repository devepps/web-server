package json;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.github.cliftonlabs.json_simple.JsonArray;

import json.jsoner.StringByte;

public enum JSONBaseType {

	STRING(String.class),
	INTEGER(Integer.class),
	LONG(Long.class),
	DOUBLE(Double.class),
	FLOAT(Float.class),
	BYTE(Byte.class),
	BOOLEAN(Boolean.class),
	BYTE_ARRAY(new byte[0].getClass()),
	STRING_BYTE(StringByte.class),
	LOCAL_DATE_TIME(LocalDateTime.class),
	LOCAL_TIME(LocalTime.class),
	LOCAL_DATE(LocalDate.class);

	private static final DataCharset DATA_CHARSET = new DataCharset();

	private Class<?> typeClass;

	private JSONBaseType(Class<?> typeClass) {
		this.typeClass = typeClass;
	}

	public Class<?> getTypeClass() {
		return typeClass;
	}

	public static JSONBaseType getBaseTypes(Class<?> typeClass) {
		for (JSONBaseType baseType : values()) {
			if (baseType.typeClass.isAssignableFrom(typeClass)) {
				return baseType;
			}
		}
		return null;
	}

	public Object getEncoded(Object object) {
		if (object == null) {
			return null;
		}
		switch (this) {
			case STRING_BYTE:
				return ((StringByte) object).getString();
			case LOCAL_DATE_TIME:
				LocalDateTime dateTime = (LocalDateTime) object;
				JsonArray jsonArray = new JsonArray();
				jsonArray.add(dateTime.getYear());
				jsonArray.add(dateTime.getMonthValue());
				jsonArray.add(dateTime.getDayOfMonth());
				jsonArray.add(dateTime.getHour());
				jsonArray.add(dateTime.getMinute());
				jsonArray.add(dateTime.getSecond());
				return jsonArray;
			case LOCAL_TIME:
				LocalTime time = (LocalTime) object;
				jsonArray = new JsonArray();
				jsonArray.add(time.getHour());
				jsonArray.add(time.getMinute());
				jsonArray.add(time.getSecond());
				return jsonArray;
			case LOCAL_DATE:
				LocalDate date = (LocalDate) object;
				jsonArray = new JsonArray();
				jsonArray.add(date.getYear());
				jsonArray.add(date.getMonthValue());
				jsonArray.add(date.getDayOfMonth());
				return jsonArray;
			case BYTE_ARRAY:
				return DATA_CHARSET.toString((byte[]) object);
			default:
				return object;
		}
	}

	public Object getDecoded(Object object) {
		if (object == null) {
			return null;
		}
		switch (this) {
			case STRING_BYTE:
				return new StringByte((String) object);
			case BOOLEAN:
				return object;
			case BYTE:
				return object;
			case DOUBLE:
				if (object.getClass().isAssignableFrom(BigDecimal.class)) {
					return ((BigDecimal) object).doubleValue();
				} else if (object.getClass().isAssignableFrom(Double.class)) {
					return object;
				}
			case FLOAT:
				if (object.getClass().isAssignableFrom(BigDecimal.class)) {
					return ((BigDecimal) object).floatValue();
				} else if (object.getClass().isAssignableFrom(Float.class)) {
					return object;
				}
			case INTEGER:
				if (object.getClass().isAssignableFrom(BigDecimal.class)) {
					return ((BigDecimal) object).intValue();
				} else if (object.getClass().isAssignableFrom(Integer.class)) {
					return object;
				}
			case LONG:
				if (object.getClass().isAssignableFrom(BigDecimal.class)) {
					return ((BigDecimal) object).longValue();
				} else if (object.getClass().isAssignableFrom(Long.class)) {
					return object;
				}
			case STRING:
				return object;
			case LOCAL_DATE_TIME:
				if (object.getClass().isAssignableFrom(JsonArray.class)) {
					JsonArray jsonArray = (JsonArray) object;
					return LocalDateTime.of(jsonArray.getInteger(0), jsonArray.getInteger(1), jsonArray.getInteger(2),
							jsonArray.getInteger(3), jsonArray.getInteger(4), jsonArray.getInteger(5));
				} else {
					return null;
				}
			case LOCAL_TIME:
				if (object.getClass().isAssignableFrom(JsonArray.class)) {
					JsonArray jsonArray = (JsonArray) object;
					return LocalTime.of(jsonArray.getInteger(0), jsonArray.getInteger(1), jsonArray.getInteger(2));
				} else {
					return null;
				}
			case LOCAL_DATE:
				if (object.getClass().isAssignableFrom(JsonArray.class)) {
					JsonArray jsonArray = (JsonArray) object;
					return LocalDate.of(jsonArray.getInteger(0), jsonArray.getInteger(1), jsonArray.getInteger(2));
				} else {
					return null;
				}
			case BYTE_ARRAY:
				if (object.getClass().isAssignableFrom(String.class)) {
					return DATA_CHARSET.toBytes((String) object);
				} else {
					return null;
				}
		}
		return null;
	}

}
